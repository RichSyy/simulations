# Simulations

This project is for storing the source codes for solving interesting 
problems by simulation.

The first game included is the coin game where player is to decide 
whether to double the pot before tossing the coin at each round. Check 
out the python program `double_pot.py` for more info.  